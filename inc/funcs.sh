wflogtime()
{
    echo "[$(date +%Y-%m-%d_%H:%M:%S)] "
}

get_ini()
{
	# use case:
	# get_ini /.secrets.ini "INI"

    local inifile=$(echo "$1" | sed "s|#basedir#|$basedir|g")
	local prefix="$2"

	declare -A declared;
    section=""
	lines=$(cat $inifile | grep -v '^\s*[;#]')
	while read -r line; do
		line=$(echo $line | tr -d '\r')
		if [[ $line == \[*] ]]
		then
			section=`echo "$line" | tr -d "[] "`
			declare -gA "$prefix$section"
			eval declared[$prefix$section]="declared"
		elif [[ $line ]]
		then
			# sed -r "s#\{timezone\}#$timezone#g" \
			# key=$(echo "$line" | sed -r 's#^([^=]+)\s*=(.*)$#|\1|#')

			# val=${line#*=}
    		# key=${line%"=$val"}

			key=$(echo "$line" | perl -pe 's#^([^=]+)(?<!\s)\s*=(.*)$#\1#g')
			val=$(echo "$line" | perl -pe 's#^([^=]+)(?<!\s)\s*=\s*(?!\s)(.*)$#\2#g')

			if ! [ ${declared[$prefix$section]} ]; then
				#declare global associative array
				declare -gA "$prefix$section"
				eval declared[$prefix$section]="declared"
			fi
            # trim quotes && apostrophes
            val=$(echo "$val" | sed 's|^\"*||g' | sed 's|\"*$||g')
            val=$(echo "$val" | sed "s|^'*||g" | sed "s|'*$||g")
			eval "$prefix$section[$key]='$val'"

		fi
	done <<< "$lines"
}

parse_options()
{
    local regex='s/(\-\-[^=]+?)-/\1_/g'
    local argString="$@"
    local -i replacementsCnt=1
    local toArray=

    lines=$(echo $argString | sed -r 's/--/\n--/g')
    while read -r line; do
		line=$(echo $line | tr -d '\r' | sed -r 's/^--//g' | sed -r 's/\s+$//g')
        if [ -z "$line" ]; then
            continue
        fi
        key=$(echo "$line" | perl -pe 's#^([^=]+)(?<!\s)\s*=(.*)$#\1#g')

        # kebab style to camelCase:
        key=$(kebab_to_camel "$key")
        val=$(echo "$line" | perl -pe 's#^([^=]+)(?<!\s)\s*=\s*(?!\s)(.*)$#\2#g')

        if [ -z "$toArray" ] && [ "$key" = "toArray" ]; then
            toArray="$val"
        fi
        # eval $key="\"$val\""
        if [ ! -z "$toArray" ]; then
            eval "$toArray['$key']='$val'"
        else
            eval $key="\"$val\""
        fi
	done <<< "$lines"
}

kebab_to_camel()
{
    declare -i replacementsCnt=1
    declare regex='s|^([^=-]+)-(.)|\1\U\2|g'
    declare toConvert="$@"
    while [ "$replacementsCnt" -gt 0 ]; do
        replacementsCnt=$(echo "$toConvert" | perl -ne "print $regex")
        toConvert=$(echo "$toConvert" | perl -pe "$regex")
    done
    echo "$toConvert"
}

get_project_dirs(){
    local includeWildcard=
    local replacePattern=
    local iterateCmd=
    local excludeBasenamePattern=
    local excludeBasenamePatterns=
    local excludePattern=
    local iterationRoot=
    local iterationName=

    for includeKey in "${!INI_IO_ITERATOR[@]}"; do
        # ignore not-"include" parameters
        (echo "$includeKey" | grep -Eq "^include") || continue

        includeWildcard="${INI_IO_ITERATOR[$includeKey]}"
        if [ -z "$includeWildcard" ]; then
            continue
        fi

        replacePattern=
        if [[ "$includeWildcard" == *'=>'* ]]; then
            replacePattern=$(echo "$includeWildcard" | sed -r 's|^.*?=>\s*(\S.*?)$|\1|' | sed -r 's|([^0-9$]+[^$]*)|"\1"|g')
            includeWildcard=$(echo "$includeWildcard" | sed -r 's|^(.*?\S)\s*=>.*?$|\1|')
        fi

        iterateCmd="find -L $includeWildcard -maxdepth 0 -type d"

        for excludeBasenameKey in "${!INI_IO_ITERATOR[@]}"; do
            (echo "$excludeBasenameKey" | grep -Eq "^exclude_basename") || continue
            excludeBasenamePatterns=$(echo "${INI_IO_ITERATOR[$excludeBasenameKey]}" | tr "," "\n")
            while read -r excludeBasenamePattern; do
                if [ -z "$excludeBasenamePattern" ]; then continue; fi
                iterateCmd="$iterateCmd -not -name \"$excludeBasenamePattern\""
            done <<< "$excludeBasenamePatterns"
        done

        for excludeKey in "${!INI_IO_ITERATOR[@]}"; do
            (echo "$excludeKey" | grep -Eq "^exclude([0-9]|$)") || continue
            excludePattern="${INI_IO_ITERATOR[$excludeKey]}"
            if [ -z "$excludePattern" ]; then continue; fi
            # iterateCmd="$iterateCmd | grep -E -v \"$excludePattern\""
            iterateCmd="$iterateCmd -not -path \"$excludePattern\""
        done

        if [ ! -z "${showFind+x}" ]; then
            # show-find
            >&2 echo -e "Iterate CMD: $iterateCmd\n"
        fi

        if [ -z "$replacePattern" ]; then
            eval $iterateCmd 2>/dev/null
        else
            while read -r iterationRoot; do
                iterationName=$(echo "$iterationRoot" | tr '/' '\n' | tac | tr '\n' '\0' | awk -F'\0' "{ print $replacePattern }")
                echo -e "$iterationRoot?/?$iterationName"
            done <<< "$(eval $iterateCmd 2>/dev/null)"
        fi
    done
}

slashes_ltrim(){
    echo "$1" | sed -r 's|^\.*\/+||g'
}

slashes_rtrim(){
    # $1 = path
    # $2 = forced (return empty string if "/")
    if [ "$2" = "1" ] || [ ! "$1" = "/" ]; then
        echo "$1" | sed -r 's|\/*$||g'
    else
        echo "$1"
    fi
}

left_characters(){
    # string, count
    echo "$1" | sed "s|\(.\{$2\}\).*|\1|"
}

path_concat(){
    # rtrim slashes:
    local path1=$(slashes_rtrim "$1" 1)
    # ltrim slashes:
    local path2=$(echo "$2" | sed 's|^\/*||g')
    local path2=$(echo "$path2" | sed 's|^\.*\/\+||g')

    slashes_rtrim "$path1/$path2"
}

init_logfile(){
    local logdir=$(echo "$1" | sed "s|#basedir#|$basedir|g")
    local logprefix="$2"

    if [ "$logdir" = "/dev/null" ]; then
        >&2 echo "`wflogtime`	Данные лога будут перенаправлены в /dev/null"
        echo "$logdir"
        return 0
    fi

    if ! [ -d $logdir ]; then
        mkdir -p $logdir
        >&2 echo "`wflogtime`	Создана папка логов $logdir"
    fi

    logName="$now.log"
    if [ ! -z "$logprefix" ]; then
        logName="$logprefix-$logName";
    fi

    logfilePath="$logdir/$logName"
    echo "`wflogtime`	Файл лога начат" > $logfilePath
    echo $logfilePath
}

test_connection(){
    local -A options
    local result=
    parse_options --to-array="options" "$@"
    ${INI_DATABASE['mysql_cmd']} -h"${options['host']}" -u"${options['user']}" --password="${options['password']}" -e exit
    if [ $? -eq 0 ]; then result="1"; else result="0"; fi

    if [ "$result" = "1" ]; then
        echo "`wflogtime`   DBConnection OK (host = ${connection['host']}, user ${connection['user']})" | tee -a $logfilePath >&2
    else
        echo "`wflogtime`   DBConnection ERROR (host = ${connection['host']}, user ${connection['user']})" | tee -a $logfilePath >&2
    fi
    echo $result
}

resolve_connection(){
    local -A options
    parse_options --to-array="options" "$@"

    resultArrayName="${options['toArray']}"
    projectRoot="${options['projectRoot']}"

    local mysqlHost=
    local mysqlUser=
    local mysqlPass=
    local mysqlDbname=
    local hostSpecificCredentials=$(echo "${options['hostSpecificCredentials']}" | sed "s|#basedir#|$basedir|g")

    if [ ! -z "$projectRoot" ]; then
        local bxSettingsPath="$projectRoot/bitrix/.settings.php"
        local dbConnPath="$projectRoot/bitrix/php_interface/dbconn.php"

        if [ -f $bxSettingsPath ]; then
            mysqlHost=$(php -d display_errors=0 -r "\$m = include '$bxSettingsPath'; echo \$m['connections']['value']['default']['host'];")
            mysqlUser=$(php -d display_errors=0 -r "\$m = include '$bxSettingsPath'; echo \$m['connections']['value']['default']['login'];")
            mysqlPass=$(php -d display_errors=0 -r "\$m = include '$bxSettingsPath'; echo \$m['connections']['value']['default']['password'];")
            mysqlDbname=$(php -d display_errors=0 -r "\$m = include '$bxSettingsPath'; echo \$m['connections']['value']['default']['database'];")
        elif [ -f $dbConnPath ]; then
            mysqlHost=$(php -d display_errors=0 -r "include(\"$dbConnPath\"); echo \$DBHost;")
            mysqlUser=$(php -d display_errors=0 -r "include(\"$dbConnPath\"); echo \$DBLogin;")
            mysqlPass=$(php -d display_errors=0 -r "include(\"$dbConnPath\"); echo \$DBPassword;")
            mysqlDbname=$(php -d display_errors=0 -r "include(\"$dbConnPath\"); echo \$DBName;")
        fi

        # parse addition connections.ini file (sections = mysql host names) - if needed
        local tmp_value=
        if [ ! -z "$mysqlHost" ] && [ ! -z "$hostSpecificCredentials" ]; then
            tmp_value=$(php -d display_errors=0 -r "\$ini=parse_ini_file(\"$hostSpecificCredentials\", true); echo \$ini['$mysqlHost']['user'];")
            if [[ ! -z "$tmp_value" ]]; then
                mysqlUser=$tmp_value
            fi
            tmp_value=$(php -d display_errors=0 -r "\$ini=parse_ini_file(\"$hostSpecificCredentials\", true); echo \$ini['$mysqlHost']['password'];")
            if [[ ! -z "$tmp_value" ]]; then
                mysqlPass=$tmp_value
            fi
        fi
    fi

    if [ -z "$mysqlHost" ]; then
        mysqlHost="${INI_DATABASE['host']}"
        mysqlUser="${INI_DATABASE['user']}"
        mysqlPass="${INI_DATABASE['pass']}"
        mysqlDbname="${INI_DATABASE['databases']}"
    fi

    eval "$resultArrayName['host']='$mysqlHost'"
    eval "$resultArrayName['user']='$mysqlUser'"
    eval "$resultArrayName['pass']='$mysqlPass'"
    eval "$resultArrayName['dbname']='$mysqlDbname'"
}

get_databases(){
    local -A options
    parse_options --to-array="options" "$@"

    local wildcards=$(echo "${options['wildcards']}" | tr "," "\n")
    local wildcard=
    local excludeWildcards=$(echo "${options['excludeWildcards']}" | tr "," "\n")
    local excludeWildcard=

    # echo "${options['host']} ${options['user']} ${options[pass]}"
    local databases=$(echo "show databases;" | ${INI_DATABASE['mysql_cmd']} --host="${options['host']}" --user="${options['user']}" --password="${options[pass]}" | tail -n +2 | grep -vE "^information_schema$" )
    # >&2 echo "alldb: $databases"
    local dbname=
    while read -r dbname; do
        while read -r wildcard; do
            if [ -z "$wildcard" ]; then continue; fi
            # $wildcard should be without quotes to be ckecked as a wildcard!!!
            if [[ "$dbname" == $wildcard ]]; then
                while read -r excludeWildcard; do
                    if [ -z "$excludeWildcard" ]; then continue; fi
                    if [[ "$dbname" == $excludeWildcard ]]; then
                        # should be excluded
                        continue 2
                    fi
                done <<< "$excludeWildcards"
                # should be accepted
                echo "$dbname"
            fi
        done <<< "$wildcards"
    done <<< "$databases"
}

get_db_dump(){
    # --host --user --pass --dbname --project-name --save-ignored-tables-structure
    local -A options
    parse_options --to-array="options" "$@"
    local dumpDataCmd="${INI_DATABASE['mysqldump_cmd']} --host=${options['host']} --user=${options['user']} --password=${options[pass]} --no-tablespaces --skip-add-drop-table --skip-add-locks --single-transaction --quick --compress -f"
    local dumpStructureCmd="echo"
    local projectName="${options['projectName']}"
    local dbName="${options['dbname']}"

    # getExcludePattern
    local excludeKey=
    local excludeWildcard=
    local excludeAllTables="0"
    local excludeTablesExpr=
    local structureOnlyExpr=

    for excludeKey in "${!INI_DATABASE[@]}"; do
        (echo "$excludeKey" | grep -Eq "^exclude_tables[0-9]*$") \
            || (echo "$excludeKey" | grep -q "^${projectName}_exclude_tables[0-9]*$") \
            || continue
        local excludeWildcards=$(echo "${INI_DATABASE[$excludeKey]}" | sed 's|\*|%|g' | tr "," "\n")
        while read -r excludeWildcard; do
            if [ -z "$excludeWildcard" ]; then continue; fi
            if [ "$excludeWildcard" = "%" ]; then
                excludeAllTables="1"
            fi
            excludeTablesExpr="$excludeTablesExpr AND Tables_in_$dbName NOT LIKE \"$excludeWildcard\""
            if [ -z "$structureOnlyExpr" ]; then
                structureOnlyExpr="Tables_in_$dbName LIKE \"$excludeWildcard\""
            else
                structureOnlyExpr="$structureOnlyExpr OR Tables_in_$dbName LIKE \"$excludeWildcard\""
            fi
        done <<< "$excludeWildcards"
    done

    if [ "$excludeAllTables" = "1" ]; then
        echo "`wflogtime`   Database '$dbName': all tables should be ignored. Mysqldump will not be performed" | tee -a $logfilePath >&2
        return 0
    fi
    if [ ! -z "$excludeTablesExpr" ] && [ "${options['saveIgnoredTablesStructure']}" = "1" ]; then
        structureOnlyExpr="SHOW TABLES WHERE ($structureOnlyExpr)"
        local structureOnlyTables=$(echo "use $dbName; $structureOnlyExpr;" | ${INI_DATABASE['mysql_cmd']} --host="${options['host']}" --user="${options['user']}" --password="${options[pass]}" | tail -n +2 | tr '\n' ' ')
        dumpStructureCmd="$dumpDataCmd --no-data $dbName $structureOnlyTables"
    fi
    local dumpRoutinesCmd="$dumpDataCmd --routines --no-data --no-create-info --skip-triggers $dbName"

    # getIncludePattern
    local includeKey=
    local includeWildcard=
    local includeAllTables="0"
    local includeTablesExpr=
    for includeKey in "${!INI_DATABASE[@]}"; do
        (echo "$includeKey" | grep -Eq "^include_tables[0-9]*$") \
            || (echo "$includeKey" | grep -q "^${projectName}_include_tables[0-9]*$") \
            || continue
        local includeWildcards=$(echo "${INI_DATABASE[$includeKey]}" | sed 's|\*|%|g' | tr "," "\n")
        while read -r includeWildcard; do
            if [ -z "$includeWildcard" ]; then continue; fi
            if [ "$includeWildcard" = "%" ]; then
                includeAllTables="1"
            fi
            if [ -z "$includeTablesExpr" ]; then
                includeTablesExpr="Tables_in_$dbName LIKE \"$includeWildcard\""
            else
                includeTablesExpr="$includeTablesExpr OR Tables_in_$dbName LIKE \"$includeWildcard\""
            fi
        done <<< "$includeWildcards"
    done

    if [ -z "$includeTablesExpr" ]; then
        echo "`wflogtime`   Database '$dbName': No include-tables pattern specified. Mysqldump will not be performed" | tee -a $logfilePath >&2
        return 0
    fi

    dumpDataCmd="$dumpDataCmd --max_allowed_packet=512M --triggers $dbName"
    if [ ! "$includeAllTables" = "1" ] || [ ! -z "$excludeTablesExpr" ]; then
        if [ -z "$includeTablesExpr" ]; then includeTablesExpr="1"; fi
        includeTablesExpr="SHOW TABLES WHERE ($includeTablesExpr) $excludeTablesExpr"
        local tablesToBackup=$(echo "use $dbName; $includeTablesExpr;" | ${INI_DATABASE['mysql_cmd']} --host="${options['host']}" --user="${options['user']}" --password="${options[pass]}" | tail -n +2 | tr '\n' ' ')
        # echo $tablesToBackup
        dumpDataCmd="$dumpDataCmd $tablesToBackup"
    fi

    # echo "Dump data CMD: $dumpDataCmd" >> $logfilePath
    # echo "Dump structure CMD: $dumpStructureCmd" >> $logfilePath
    # { echo "one line"; echo "second line"; }
    { $dumpRoutinesCmd; $dumpDataCmd; $dumpStructureCmd; } \
    | sed -e "s#\/\*\![[:digit:]]*\s*DEFINER.*\*\/##" \
    | sed -e "s#DEFINER\s*=\s*[^*]*\(FUNCTION\|PROCEDURE\)#\1#" \
    | sed 's/ENGINE=MyISAM/ENGINE=InnoDB/'
    # | sed 's/CHARSET=utf8mb4/CHARSET=utf8/' \
    # | sed 's/COLLATE utf8mb4/COLLATE utf8/' \
    # | sed 's/COLLATE=utf8mb4/COLLATE=utf8/'
}

get_compression_cmd(){
    local level="7"
    local cmd=
    if [ ! "${INI_COMPRESSION['enabled']}" = "1" ]; then
        echo "$passOn"
        return 0
    fi
    if [ ! -z "${INI_COMPRESSION['level']}" ]; then
        level="${INI_COMPRESSION['level']}"
    fi
    if [ ! -z "${INI_COMPRESSION['cmd']}" ]; then
        echo "${INI_COMPRESSION['cmd']}"
        return 0
    fi
    if [ "${INI_COMPRESSION['engine']}" = "gzip" ] || [ "${INI_COMPRESSION['engine']}" = "bzip2" ] || [ "${INI_COMPRESSION['engine']}" = "brotli" ]; then
        echo "${INI_COMPRESSION['engine']} -$level -c"
        return 0
    fi
    if [ "${INI_COMPRESSION['engine']}" = "zstd" ]; then
        cmd="zstd -$level -c"
        if [ ! -z "${INI_COMPRESSION['threads']}" ]; then
            cmd+=" -T${INI_COMPRESSION['threads']}"
        fi
        echo "$cmd"
        return 0
    fi

    echo "$passOn"
}

get_encryption_cmd(){
    if [ ! "${INI_ENCRYPTION['enabled']}" = "1" ]; then
        echo "$passOn"
        return 0
    fi
    echo "openssl enc -e -aes-256-cbc -k ${INI_ENCRYPTION['pass']}"
}

get_output_cmd(){
    local now=$(date +%Y-%m-%d_%H.%M.%S)
    local name="$1-$now"
    local ext="$2"
    # rtrim slashes:
    local outputDir=$(echo "${INI_OUTPUT['dir']}" | sed 's|\/*$||g' | sed "s|#basedir#|$basedir|g")
    local remoteFilepath=
    local outputCmd=

    if [ ! -z "$ext" ]; then
        name="$name.$ext"
    fi

    if [ "${INI_COMPRESSION['enabled']}" = "1" ]; then
        name="$name.${INI_COMPRESSION['extension']}"
    fi
    if [ "${INI_ENCRYPTION['enabled']}" = "1" ]; then
        name="$name.aes"
    fi

    remoteFilepath="$outputDir/$name"
    outputCmd="$(dirname $0)/bin/gateway.php"
    outputCmd="php -d mbstring.func_overload=0 -d allow_url_fopen=1 $outputCmd"
    if [ "${INI_OUTPUT['host']}" = "localhost" ]; then
        outputCmd="$outputCmd -s 1 -l 0 -r 8K -M 0 -f $remoteFilepath"
        if [ ! -d "$outputDir" ]; then
            mkdir -p $outputDir
            echo "`wflogtime`	Создана папка $outputDir" | tee -a $logfilePath >&2
        fi
    else
        outputCmd="$outputCmd -s 1 -l 10G -r 8K -L $logfilePath -M 100 -f $remoteFilepath -h ${INI_OUTPUT['host']} -u ${INI_OUTPUT['user']} -p ${INI_OUTPUT['pass']}"
    fi
    echo "$outputCmd"
}

get_shell_targets(){
    local projectRoot=$(realpath "$1")
    local projectName="$2"
    local allowSpecTargetsAlone="$3"
    local includeString=
    local pwdBefore=$(pwd)
    local targetsToBeAdded=
    local targetsToBeExcluded=

    projectRoot=$(slashes_rtrim "$projectRoot")

    # getExcludeBasenameTargets
    # local excludeKey=
    # local excludeWildcard=
    # local excludeWildcards=
    # local excludeAll="0"
    # local excludeString=
    # for excludeKey in "${!INI_PROJECT[@]}"; do
    #     (echo "$excludeKey" | grep -Eq "^exclude_basename[0-9]*$") \
    #         || (echo "$excludeKey" | grep -q "^${projectName}_exclude_basename[0-9]*$") \
    #         || continue
    #     excludeWildcards=$(echo "${INI_PROJECT[$excludeKey]}" | tr "," "\n")
    #     while read -r excludeWildcard; do
    #         if [ -z "$excludeWildcard" ]; then continue; fi
    #         if [ "$excludeWildcard" = "*" ]; then
    #             excludeAll="1";
    #             break
    #         fi
    #         excludeString="$excludeString -not -name '$excludeWildcard'"
    #     done <<< "$excludeWildcards"
    # done

    local excludeKey=
    local excludeWildcard=
    local excludeWildcards=
    local excludeAll="0"
    local excludeString=
    for excludeKey in "${!INI_PROJECT[@]}"; do
        (echo "$excludeKey" | grep -Eq "^exclude[0-9]*$") \
            || (echo "$excludeKey" | grep -q "^${projectName}_exclude[0-9]*$") \
            || continue
        excludeWildcards=$(echo "${INI_PROJECT[$excludeKey]}" | tr "," "\n")
        while read -r excludeWildcard; do
            if [ -z "$excludeWildcard" ]; then continue; fi
            if [ "$excludeWildcard" = "." ] || [ "$excludeWildcard" = "*" ] || [ "$excludeWildcard" = "./" ] || [ "$excludeWildcard" = "./*" ]; then 
                excludeAll="1";
                break
            fi
            # excludeString="$excludeString -not -path '$excludeWildcard'"
            excludeString="$excludeString --exclude='$excludeWildcard'"
            targetsToBeExcluded="$targetsToBeExcluded $excludeWildcard"
        done <<< "$excludeWildcards"
    done

    if [ "$excludeAll" = "1" ] && [ ! "$allowSpecTargetsAlone" = "1" ]; then
        echo -e "\t\t\t\t=> Files of '$projectName' should be ignored" | tee -a $logfilePath >&2
        targetsToBeExcluded="*"
        # if [ ! -z "${test+x}" ]; then
        #     # test mode
        #     echo -e "\ttargetsToBeAdded: $targetsToBeAdded\n\ttargetsToBeExcluded: $targetsToBeExcluded" | tee -a $logfilePath >&2
        # fi
        return 0
    fi

    # getIncludeTargets
    local includeKey=
    local includeWildcard=
    local includeWildcards=
    local includeAll="0"
    local includeTargets=
    local target=
    for includeKey in "${!INI_PROJECT[@]}"; do
        (echo "$includeKey" | grep -Eq "^include[0-9]*$") \
            || (echo "$includeKey" | grep -q "^${projectName}_include[0-9]*$") \
            || continue
        includeWildcards=$(echo "${INI_PROJECT[$includeKey]}" | tr "," "\n")
        while read -r includeWildcard; do
            if [ -z "$includeWildcard" ]; then continue; fi
            if [ "$includeWildcard" = "." ] || [ "$includeWildcard" = "*" ] || [ "$includeWildcard" = "./" ] || [ "$includeWildcard" = "./*" ]; then 
                includeAll="1";
                break
            fi
            includeWildcard=$(slashes_ltrim $includeWildcard)
            includeTargets=$(cd $projectRoot && find -L $includeWildcard -maxdepth 0 2>/dev/null)
            while read -r target; do
                if [ -z "$target" ]; then continue; fi
                target=$(slashes_ltrim "$target")
                target=$(echo "`path_concat \"$projectRoot\" \"$target\"`:$target" | base64 -w 0)
                includeString="$includeString --shell-target='base64:$target'"
                # includeString="$includeString --shell-target='`path_concat "$projectRoot" "$target"`:$target'"
                targetsToBeAdded="$targetsToBeAdded $target"
            done <<< "$includeTargets"
        done <<< "$includeWildcards"
    done

    if [ "$includeAll" = "1" ]; then
        includeString="--shell-target=\"$projectRoot:/\""
        targetsToBeAdded="*"
    fi

    # special targets
    if [ ! -z "$includeString" ] || [ "$allowSpecTargetsAlone" = "1" ]; then
        declare pathFrom=
        declare pathTo=
        for includeKey in "${!INI_PROJECT[@]}"; do
            (echo "$includeKey" | grep -Eq "^include_special[0-9]*$") \
                || (echo "$includeKey" | grep -q "^${projectName}_include_special[0-9]*$") \
                || continue
            includeWildcards=$(echo "${INI_PROJECT[$includeKey]}" | tr "," "\n")
            while read -r includeWildcard; do
                if [ -z "$includeWildcard" ]; then continue; fi
                if [ "$includeWildcard" = "." ] || [ "$includeWildcard" = "*" ] || [ "$includeWildcard" = "./" ] || [ "$includeWildcard" = "./*" ] || [ "$includeWildcard" = "/" ]; then 
                    # unexpected case
                    break
                fi

                if [[ "$includeWildcard" == *"#basedir#"* ]]; then
                    includeWildcard=$(echo "$includeWildcard" | sed "s|#basedir#|`realpath $basedir`|g")
                    # includeWildcard=$(realpath "$includeWildcard")
                fi

                if [ ! "$(left_characters "$includeWildcard" 2)" = "./" ] && [ ! "$(left_characters "$includeWildcard" 1)" = "/" ]; then
                    includeWildcard="./$includeWildcard"
                fi

                if [ "$(left_characters "$includeWildcard" 2)" = "./" ]; then
                    # concern targets as relative from project root
                    includeWildcard=$(slashes_ltrim $includeWildcard)
                    includeTargets=$(cd $projectRoot && find -L $includeWildcard -maxdepth 0 2>/dev/null)
                    while read -r target; do
                        if [ -z "$target" ]; then continue; fi
                        target=$(slashes_ltrim "$target")
                        includeString="$includeString --php-target=\"$projectRoot/$target:$target\""
                        targetsToBeAdded="$targetsToBeAdded $target"
                    done <<< "$includeTargets"
                elif [ "$(left_characters "$includeWildcard" 1)" = "/" ]; then
                    # concern targets as absolute paths
                    pathFrom="$includeWildcard"
                    pathTo="$includeWildcard"
                    if [[ "$includeWildcard" == *'=>'* ]]; then
                        pathFrom=$(echo "$includeWildcard" | sed -r 's|^(.*?\S)\s*=>.*?$|\1|')
                        pathTo=$(echo $includeWildcard | sed -r 's|^.*?=>\s*(\S.*?)$|\1|')
                    fi
                    includeString="$includeString --php-target=\"$pathFrom:$pathTo\""
                    targetsToBeAdded="$targetsToBeAdded $pathTo"
                fi
            done <<< "$includeWildcards"
        done
    fi

    cd $pwdBefore
    # includeString="$includeString $excludeString"
    if [ ! -z "${test+x}" ]; then
        # test mode
        echo -e "\ttargetsToBeAdded: $targetsToBeAdded\n\ttargetsToBeExcluded: $targetsToBeExcluded" | tee -a $logfilePath >&2
    fi
    if [ -z "$includeString" ]; then includeString=" "; fi;
    if [ -z "$excludeString" ]; then excludeString=" "; fi;
    { echo "$includeString"; echo "$excludeString"; }
}

get_project_name(){
    local iterationRoot="$1"
    local projectRoot="$2"
    local iterationRoot="$3"
    # >&2 echo "iterationRoot: $iterationRoot; projectRoot: $projectRoot"
    if [ -z "${INI_PROJECT[name]}" ]; then
        basename $iterationRoot
        return 0
    fi

    local template=$(echo "${INI_PROJECT[name]}" | sed "s|#iteration_root#|$iterationRoot|g" | sed "s|#project_root#|$projectRoot|g" | sed "s|#iteration_name#|$iterationName|g")
    local name=
    if [[ "$template" == *'=>'* ]]; then
        local path=$(echo "$template" | sed -r 's|^(.*?\S)\s*=>.*?$|\1|')
        local partNumber=$(echo $template | sed -r 's|^.*?=>\s*(\S.*?)$|\1|')
        if (( "$partNumber" < 1 )); then partNumber="1"; fi
        name=$(echo "$path" | tr '/' '\n' | tail -$partNumber | head -1)
    else
        name=$(echo "$template")
    fi

    if [ -z "$name" ]; then
        basename $iterationRoot
    elif [ ! -z "${INI_PROJECT_ALIASES[$name]}" ]; then
        echo "${INI_PROJECT_ALIASES[$name]}"
    else
        echo "$name"
    fi
}

display_time(){
    local T=$1
    local D=$((T/60/60/24))
    local H=$((T/60/60%24))
    local M=$((T/60%60))
    local S=$((T%60))
    (( $D > 0 )) && printf '%d days ' $D
    (( $H > 0 )) && printf '%d hours ' $H
    (( $M > 0 )) && printf '%d minutes ' $M
    (( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
    printf '%d seconds\n' $S
}

bytes_to_human_readable(){
    local i=${1:-0} d="" s=0 S=("Bytes" "KiB" "MiB" "GiB" "TiB" "PiB" "EiB" "YiB" "ZiB")
    while ((i > 1024 && s < ${#S[@]}-1)); do
        printf -v d ".%02d" $((i % 1024 * 100 / 1024))
        i=$((i / 1024))
        s=$((s + 1))
    done
    echo "$i$d ${S[$s]}"
}
