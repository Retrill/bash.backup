<?php
namespace Webformat;

function getCanonicalBytes($value){
    $value = str_replace(array(',', ' '), array('.', ''), ($value));
    if(!preg_match('#^([\d\.]+)([A-Za-z]+)$#', $value, $matches)){
        return (float)$value;
    }
    $measureUnit = strtoupper($matches[2]);
    $value = (float)$matches[1];

    switch($measureUnit){
        case 'T': $value *= 1024;
        case 'G': $value *= 1024;
        case 'M': $value *= 1024;
        case 'K': $value *= 1024;
        case 'B': break;
        default: throw new \Exception('Unexpected measure unit "'.$measureUnit.'"!');
    }
    return $value;
}

unset($argv[0]); //filepath

$sum = 0;
foreach($argv as $memoryValue){
    $sum += getCanonicalBytes($memoryValue);
}

echo $sum;