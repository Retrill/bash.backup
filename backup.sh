#!/bin/bash
# options:
# --profile
# --logname
# --test
# --list-projects
# --show-find
# --single-databases
# --project

scriptStart=`date +%s`
# iniFilepath=/home/bitrix/backup/inc/backup.ini

now=$(date +%Y-%m-%d_%H.%M.%S)
basedir=$(dirname $0)
tar="$basedir/bin/tar.php"
# passOn="awk '{print \$0}'"
passOn="php $basedir/bin/pass-on.php"
. "$basedir/inc/funcs.sh"

parse_options "$@"

if [ -z "$profile" ]; then
    echo "No profile specified!"
    exit 1
fi

profiles=$(echo $profile | tr "," "\n")
declare -A connection;
declare compressionCmd=
declare encryptionCmd=
declare outputCmd=
declare dbDumpCmd=
declare projectRoot=
declare projectName=
declare projectIncludeExclude=
declare projectExcludeTargets=
declare projectIncludeTargets=
declare tarMemoryLimit=
declare ignoreDb=
declare reportMsg=
declare uniqueDbKey=
declare projectShouldBeIngored=
declare -A dumpedDatabases
declare projectsCnt=0
declare projectNum=0
declare iterationRoot=
declare iterationName=
declare stat=
declare projectBytes=
declare projectSpeed=
declare sumBytes=0
declare avgSpeed=0


for profile in $profiles; do
    # cleaning previously included INI-settings first:
    for varname in $(compgen -A variable | grep "INI_"); do
        unset $varname
    done

    # read current profile ini-file
    iniFilepath="$(dirname $0)/profiles/$profile.ini"
    if [ ! -f $iniFilepath ]; then
        echo "Profile ini-file does not exist ($profile)"
        continue
    fi
    get_ini $iniFilepath "INI_"
    if [ ! -z "${INI_GENERAL['env_file']}" ]; then
        get_ini "${INI_GENERAL['env_file']}" "INI_"
    fi

    if [ -z "$logfilePath" ]; then
        logfilePath=$(init_logfile "${INI_GENERAL['logs_dir']}" "$logname")
    fi
    echo "logFilePath: $logfilePath"

    echo -e "\n`wflogtime`   --- Start to processing '$profile' profile ---" | tee -a $logfilePath
    compressionCmd=$(get_compression_cmd)
    encryptionCmd=$(get_encryption_cmd)

    tarMemoryLimit="512M"
    if ! [ -z "${INI_DATABASE['memory_buffer']}" ]; then
        tarMemoryLimit=$(php $basedir/bin/memory_sum.php ${INI_DATABASE['memory_buffer']} 128M)
    fi

    iterations_cnt="$(get_project_dirs | wc -l)"
    if [ "$iterations_cnt" = "0" ]; then
        echo "`wflogtime`       => Database only mode (no project files will be backuped)" | tee -a $logfilePath
        connection=()
        resolve_connection --to-array="connection" --project-root="" --host-specific-credentials="${INI_DATABASE['use_host_specific_credentials']}"
        test_conn_result=$(test_connection --host="${connection['host']}" --user="${connection['user']}" --password="${connection[pass]}")
        if [ "$test_conn_result" = "1" ]; then
            databasesToDump=$(get_databases --wildcards="${connection['dbname']}" --exclude-wildcards="${INI_DATABASE['exclude_databases']}" --host="${connection['host']}" --user="${connection['user']}" --pass="${connection[pass]}")
            if [ ! -z "${test+x}" ] || [ ! -z "${listProjects+x}" ]; then
                # test mode
                echo "Databases to be dumped:"
            fi
            # while read -r dbname; do
            for dbname in $databasesToDump; do
                if [ ! -z "${project+x}" ] && [ ! "$project" = "$dbname" ]; then
                    continue
                fi
                ignoreDb=
                uniqueDbKey="${dbname}@${connection[host]}"
                if [ ! -z "${singleDatabases+x}" ] && [ ! -z "${dumpedDatabases[$uniqueDbKey]}" ]; then
                    ignoreDb="1"
                else
                    projectNum=$((projectNum + 1))
                fi
                if [ ! -z "${test+x}" ] || [ ! -z "${listProjects+x}" ]; then
                    # test mode
                    reportMsg="$dbname"
                    if [ "$ignoreDb" = "1" ]; then
                        reportMsg+=" => should be ignored (${dumpedDatabases[$uniqueDbKey]})"
                    else
                        reportMsg="$projectNum: $reportMsg"
                    fi
                    echo "$reportMsg"
                else
                    if [ "$ignoreDb" = "1" ]; then
                        echo "`wflogtime`   Database '$dbname' should be ignored (${dumpedDatabases[$uniqueDbKey]})" | tee -a $logfilePath
                    else
                        echo "`wflogtime`   Start dumping database '$dbname' ($projectNum)" | tee -a $logfilePath
                        outputCmd=$(get_output_cmd "$profile-$dbname" "tar")

                        dbDumpCmd="get_db_dump --host=\"${connection['host']}\" \
                            --user=\"${connection['user']}\" \
                            --pass=\"${connection['pass']}\" --dbname=\"$dbname\"
                            --project-name=\"\" \
                            --save-ignored-tables-structure=\"1\""
                        specialTargets=$(get_shell_targets "/dev/null" "$dbname" 1)

                        # get_db_dump --host="${connection['host']}" --user="${connection['user']}" \
                        #     --pass="${connection['pass']}" --dbname="$dbname" --project-name="" \
                        #     --save-ignored-tables-structure="1"
                        stat=$(eval $dbDumpCmd \
                        | php -d mbstring.func_overload=0 -d memory_limit=$tarMemoryLimit $tar \
                            --stdin-read-step=2M \
                            --stdin-memory-buffer="${INI_DATABASE['memory_buffer']}" \
                            --php-target="#stdin#:${INI_DATABASE['dump_to']}" \
                            $specialTargets \
                        | $compressionCmd | $encryptionCmd | $outputCmd)
                        projectBytes=$(echo "$stat" | head -1)
                        projectSpeed=$(echo "$stat" | tail -1)
                        sumBytes=$((sumBytes + projectBytes))
                        echo -e "\t`wflogtime`   FINISHED dumping database '$dbname' ($projectNum)" | tee -a $logfilePath
                    fi
                fi
                dumpedDatabases["$uniqueDbKey"]="already dumped by profile $profile"
            done
            # done <<< "$databasesToDump"
        fi
    else
        # iterate throughout projects
        while read -r iterationRoot; do
            if [ -z "$iterationRoot" ]; then continue; fi
            iterationName=$(basename "$iterationRoot")
            if [[ "$iterationRoot" == *'?/?'* ]]; then
                iterationRoot=$(echo "$iterationRoot" | sed 's|?/?|\n|')
                iterationName=$(echo "$iterationRoot" | tail -1)
                iterationRoot=$(echo "$iterationRoot" | head -1)
            fi
            projectRoot=$(path_concat "$iterationRoot" "${INI_PROJECT['root']}")
            projectName=$(get_project_name "$iterationRoot" "$projectRoot" "$iterationName")
            if [ ! -z "${project+x}" ] && [ ! "$project" = "$projectName" ]; then
                continue
            fi
            if [ -z "$(ls -A $projectRoot)" ]; then
                echo "$projectRoot is empty directory, ignoring..."
                continue
            fi

            projectNum=$((projectNum + 1))
            if [ ! -z "${listProjects+x}" ]; then
                # list-projects mode
                echo "$projectNum: $projectRoot => $projectName (iteration name: $iterationName)"
                continue
            else
                echo -e "\n`wflogtime`   $projectNum: Iterating to '$iterationRoot'" | tee -a $logfilePath
            fi

            echo -e "`wflogtime`   Project root to be processed: '$projectRoot' => $projectName" | tee -a $logfilePath

            # get_db_dump $projectRoot
            connection=()
            resolve_connection --to-array="connection" --project-root="$projectRoot" --host-specific-credentials="${INI_DATABASE['use_host_specific_credentials']}"
            ignoreDb=
            dbDumpCmd="echo"
            uniqueDbKey="${connection[dbname]}@${connection[host]}"
            if [ ! -z "${singleDatabases+x}" ] && [ ! -z "${dumpedDatabases[$uniqueDbKey]}" ]; then
                echo -e "\t\t\t\t=> Database '${connection[dbname]}' should be ignored (${dumpedDatabases[$uniqueDbKey]})" | tee -a $logfilePath
            else
                if [ -z "${connection['host']}" ] && [ -z "${connection['user']}" ]; then
                    echo -e "\t\t\t\t=> Database host && user are not specified, ignoring database dumping for project" | tee -a $logfilePath
                    test_conn_result="0"
                else
                    test_conn_result=$(test_connection --host="${connection['host']}" --user="${connection['user']}" --password="${connection[pass]}")
                fi

                if [ "$test_conn_result" = "1" ]; then
                    echo "`wflogtime`   Database to be dumped: '${connection['dbname']}'" | tee -a $logfilePath
                    if [ -z "${test+x}" ]; then
                        # production mode
                        dbDumpCmd="get_db_dump --host=\"${connection['host']}\" --user=\"${connection['user']}\" --pass=\"${connection['pass']}\" --dbname=\"${connection['dbname']}\" --project-name=\"$projectName\" --save-ignored-tables-structure=\"1\""
                    fi
                fi
            fi

            projectIncludeExclude=$(get_shell_targets "$projectRoot" "$projectName")
            projectIncludeTargets=$(echo "$projectIncludeExclude" | head -1 | sed 's|^[[:blank:]]*||g')
            projectExcludeTargets=$(echo "$projectIncludeExclude" | tail -1 | sed 's|^[[:blank:]]*||g')
            projectShouldBeIngored=
            if [ -z "$projectIncludeTargets" ] && [ "$dbDumpCmd" = "echo" ] && [ -z "${test+x}" ] && [ -z "${listProjects+x}" ]; then
                projectShouldBeIngored="1"
                echo -e "\t\t\t\t=> Project should be totally ignored (no file targets && databases selected)" | tee -a $logfilePath
            fi

            if [ -z "${test+x}" ] && [ -z "${listProjects+x}" ] && [ ! "$projectShouldBeIngored" = "1" ]; then
                # production mode
                outputCmd=$(get_output_cmd "$profile-$projectName" "tar")
                stat=$(eval $dbDumpCmd \
                | php -d mbstring.func_overload=0 -d memory_limit=$tarMemoryLimit $tar \
                    --stdin-read-step=2M \
                    --stdin-memory-buffer="${INI_DATABASE['memory_buffer']}" \
                    --php-target="#stdin#:${INI_DATABASE['dump_to']}" \
                    $projectIncludeTargets $projectExcludeTargets \
                | $compressionCmd | $encryptionCmd | $outputCmd)
                projectBytes=$(echo "$stat" | head -1)
                projectSpeed=$(echo "$stat" | tail -1)
                sumBytes=$((sumBytes + projectBytes))
            fi

            if [ "$test_conn_result" = "1" ] && [ -z "${dumpedDatabases[$uniqueDbKey]}" ]; then
                dumpedDatabases["$uniqueDbKey"]="already dumped at project $projectRoot"
            fi
        done <<< "$(get_project_dirs)"
    fi
done

echo -e "`wflogtime`   Done\n"
echo "Sum data length: $(bytes_to_human_readable $sumBytes)" | tee -a $logfilePath

# time in seconds
sumTime=$((`date +%s` - scriptStart))
echo "Execution time: $(display_time $sumTime)" | tee -a $logfilePath

avgSpeed=$((sumBytes / sumTime))
echo "Average speed: $(bytes_to_human_readable $avgSpeed) / sec" | tee -a $logfilePath
echo -e "\n"
